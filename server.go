///bin/true; exec /usr/bin/env go run "$0" "$@"
// shebang from https://stackoverflow.com/a/30082862

package main

import (
	"database/sql"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	_ "github.com/lib/pq"
	"github.com/microcosm-cc/bluemonday"
	"gopkg.in/gomail.v2"
)

type messageInformation struct {
	Name            string
	Email           string
	Message         string
	RecvTime        time.Time
	HTMLInformation struct {
		XRealIP       string
		XForwardedFor string
		Host          string
		Origin        string
		Referer       string
		UserAgent     string
		URL           string
	}
}

var smtpInfo struct {
	Server   string
	Port     int
	User     string
	Password string
	From     string
	To       string
	Subject  string
}

var smtpDialer *gomail.Dialer
var db *sql.DB

var policyNameEmail *bluemonday.Policy
var policyMessage *bluemonday.Policy

func sendEmail(s messageInformation) {
	var body strings.Builder
	body.WriteString("<pre>Sender Information:\r\n  Name: ")
	body.WriteString(s.Name)
	body.WriteString("\r\n  Email: ")
	body.WriteString(s.Email)
	body.WriteString("\r\n  Time: ")
	body.WriteString(s.RecvTime.Format(time.RFC822))
	body.WriteString("\r\n  IP: ")
	body.WriteString(s.HTMLInformation.XRealIP)
	body.WriteString("\r\n  Page: ")
	body.WriteString(s.HTMLInformation.Referer)
	body.WriteString("</pre>\r\n")

	body.WriteString(s.Message)

	m := gomail.NewMessage()
	m.SetHeader("From", smtpInfo.From)
	m.SetHeader("To", smtpInfo.To)
	m.SetHeader("Subject", smtpInfo.Subject)
	m.SetBody("text/html", body.String())

	log.Printf("Sending email to %s from %s", smtpInfo.To, smtpInfo.From)

	if err := smtpDialer.DialAndSend(m); err != nil {
		log.Fatal(err)
	}
}

func handleRoot(w http.ResponseWriter, r *http.Request) {

	w.Header().Add("Access-Control-Allow-Origin", "*")
	w.Header().Add("Cache-Control", "no-cache, no-store, must-revalidate")
	w.Header().Add("Pragma", "no-cache")
	w.Header().Add("Expires", "0")

	switch r.Method {
	case "POST":
		// Call ParseForm() to parse the raw query and update r.PostForm and r.Form.
		if err := r.ParseForm(); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		m := messageInformation{}

		m.Name = policyNameEmail.Sanitize(r.PostFormValue("name"))
		m.Email = policyNameEmail.Sanitize(r.PostFormValue("email"))
		m.Message = policyMessage.Sanitize(r.PostFormValue("message"))
		m.RecvTime = time.Now()
		m.HTMLInformation.XRealIP = r.Header.Get("X-Real-IP")
		m.HTMLInformation.XForwardedFor = r.Header.Get("X-Forwarded-For")
		m.HTMLInformation.Host = r.Host
		m.HTMLInformation.Origin = r.Header.Get("Origin")
		m.HTMLInformation.Referer = r.Header.Get("Referer")
		m.HTMLInformation.UserAgent = r.Header.Get("User-Agent")
		m.HTMLInformation.URL = r.URL.RawQuery

		// make sure ip is valid since it's not a string
		if net.ParseIP(m.HTMLInformation.XRealIP) == nil {
			m.HTMLInformation.XRealIP = "0.0.0.0"
			log.Print("Bad sender ip. Ignoring with '0.0.0.0'...")
		}

		badname := len(m.Name) < 3 || len(m.Name) > 100
		bademail := len(m.Email) < 8 || len(m.Email) > 100
		badmessage := len(m.Message) < 10 || len(m.Message) > 10000

		if len(m.HTMLInformation.Referer) > 1024 {
			log.Print("Referer too long")
			http.Error(w, "Referer too long", http.StatusBadRequest)
			return
		}

		if badname || bademail || badmessage {
			var data string = ""

			if badname {
				data += "badname \r\n"
			}

			if bademail {
				data += "bademail \r\n"
			}

			if badmessage {
				data += "badmessage \r\n"
			}

			http.Error(w, data, http.StatusBadRequest)
			return
		}

		var err error
		var rows *sql.Rows
		rows, err = db.Query("SELECT COUNT(id) FROM comments WHERE time >= now()::date")
		if err != nil {
			log.Print(err)
			http.Error(w, "", http.StatusInternalServerError)
			return
		}
		var messagesToday int
		if rows.Next() {
			var s string
			if err := rows.Scan(&s); err != nil {
				log.Fatal(err)
			}
			messagesToday, _ = strconv.Atoi(s)
		}
		rows, err = db.Query("SELECT COUNT(id) FROM comments WHERE sendingip=$1 AND time >= now()::date", m.HTMLInformation.XRealIP)
		if err != nil {
			log.Print(err)
			http.Error(w, "", http.StatusInternalServerError)
			return
		}
		var messagesFromIPToday int
		if rows.Next() {
			var s string
			if err := rows.Scan(&s); err != nil {
				log.Fatal(err)
			}
			messagesFromIPToday, _ = strconv.Atoi(s)
		}

		log.Printf("Message %d from today (%d from this sender)", messagesToday, messagesFromIPToday)

		if messagesFromIPToday > 10 || messagesToday > 100 {
			http.Error(w, "too many requests", http.StatusForbidden)
			log.Print("rate limiting, ignoring request")
			return
		}

		rows, err = db.Query("INSERT INTO comments VALUES (DEFAULT, $1, $2, $3, $4, $5, $6)", m.Name, m.Email, m.HTMLInformation.XRealIP, m.HTMLInformation.Referer, m.RecvTime.Format(time.RFC3339), m.Message)
		if err != nil {
			log.Fatal(err)
		}

		sendEmail(m)

	default:
		http.Error(w, "Request not supported", http.StatusInternalServerError)
	}
}

func main() {
	// don't allow any html
	policyNameEmail = bluemonday.StrictPolicy()

	// allow "safe for user content" html tags
	policyMessage = bluemonday.UGCPolicy()

	smtpInfo.Server = os.Getenv("SCFES_SMTP_SERVER")
	smtpInfo.Port, _ = strconv.Atoi(os.Getenv("SCFES_SMTP_PORT"))
	smtpInfo.User = os.Getenv("SCFES_SMTP_USER")
	smtpInfo.Password = os.Getenv("SCFES_SMTP_PASS")
	smtpInfo.From = os.Getenv("SCFES_FROM")
	smtpInfo.To = os.Getenv("SCFES_TO")
	smtpInfo.Subject = os.Getenv("SCFES_SUBJECT")

	smtpDialer = gomail.NewDialer(smtpInfo.Server, smtpInfo.Port, smtpInfo.User, smtpInfo.Password)

	pgConnStr := os.Getenv("SCFES_PG_CONNSTR")
	pg, err := sql.Open("postgres", pgConnStr)
	if err != nil {
		log.Fatal(err)
	}
	db = pg

	listenaddr := os.Getenv("SCFES_LISTEN")

	http.HandleFunc("/", handleRoot)
	fmt.Printf("Starting server...\n")
	if err := http.ListenAndServe(listenaddr, nil); err != nil {
		log.Fatal(err)
	}
}
